//
// Created by codeforcesrankzero on 20.09.2022.
//
#include "StringLongNum.h"
#include "BitLongNum.h"
#include <string>
#include "Factorials.h"
class Handler{
public:
    static void HandleInput(int argc, char** argv);
    static void RunTests(long long n, long long digits = 200);
    static void SimplyCountFactorial(long long n, long long digits = 200);
};

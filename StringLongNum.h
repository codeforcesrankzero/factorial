#pragma once
#include <iostream>
#include <vector>
class StringLongNum{

private:
    std::string value;

public:
    size_t length(){
        return this->value.length();
    }

    StringLongNum substr_out(size_t n);
    //в конструктор передаем ссылку, лишнего копирования не происходит
    StringLongNum(const std::string& val): value(val){
        //при инициализации будем резервировать место для избежания аллокаций
        value.reserve(10000);
    }

    friend std::ostream& operator<<(std::ostream& os, StringLongNum& longNum);

    friend std::ostream& operator<<(std::ostream& os, StringLongNum longNum);

    friend StringLongNum operator+(const StringLongNum& left, const StringLongNum& right);

    friend StringLongNum operator*(const StringLongNum& left, const StringLongNum& right);
    friend StringLongNum operator*(const StringLongNum& left, const int right);

    bool operator == (const StringLongNum& val){
        return val.value == this->value;
    }

};
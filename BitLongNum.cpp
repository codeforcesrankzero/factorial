#include "BitLongNum.h"

size_t BitLongNum::size(){
    return inner_repr.size();
}

BitLongNum::BitLongNum(std::vector<int>& vec) {
    inner_repr = vec;
    inner_repr.reserve(10000);
    //при инициализации выделяем память, чтобы избежать аллокаций
}

BitLongNum::BitLongNum(const std::string& input_string){
    int base = 1000;
    unsigned int dig = 1;
    int current_val = 0;
    inner_repr.reserve(10000);
    //аналогично выделяем память,чтобы избежать аллокаций
    for (auto it = input_string.crbegin(); it != input_string.crend(); ++it) {
        current_val += (*it - '0') * dig;
        dig *= 10;
        if (dig == base) {
            inner_repr.emplace_back(current_val);
            current_val = 0;
            dig = 1;
        }
    }

    if (current_val != 0) {
      inner_repr.emplace_back(current_val);
    }
}

void BitLongNum::normalize(){
    //только чтение и запись
    int base = 1000;
    for (auto i = 0; i < inner_repr.size(); ++i) {
        inner_repr[i + 1] += inner_repr[i] / base;
        inner_repr[i] %= base;
    }
    int i = inner_repr.size() - 1;
    while (i >= 0 and inner_repr[i] == 0){
        inner_repr.pop_back();
        --i;
    }
}

void BitLongNum::extend(int len){
    //только чтение и запись(экстенд будет записывать в уже выделенную во внутреннем представлении память
    while (len & (len - 1)) {
        ++len;
    }

    inner_repr.resize(len);
}
BitLongNum BitLongNum::operator*(const BitLongNum& other) const{
    auto len = this->inner_repr.size();
    std::vector<int> res(2 * len);
    //инициализируем вектор с излишней длинной, далее запись и чтение
    for (auto i = 0; i < len; ++i) {
        for (auto j = 0; j < len; ++j) {
            res[i + j] += this->inner_repr[i] * other.inner_repr[j];
        }
    }
    //при инициализации сделаем резерв
    return BitLongNum(res);
}
void BitLongNum::printRes(int digits) {
    //только чтение
    auto it = inner_repr.crbegin();
    while (!*it) {
        ++it;
    }
    bool not_first = false;
    int counter = 0;
    while (it != inner_repr.crend() and counter < digits) {
        int zeros_missing = -1;
        int num = *it;

        if (num == 0) {
            num += 1;
        }
        if (not_first and num < 100) {
            zeros_missing = 1;

            while ((num *= 10) < 100) {
                ++zeros_missing;
            }
        }

        if (zeros_missing > 0) {
            while (zeros_missing--) {
                std::cout << '0';
                ++counter;
                if(counter >= digits - 1){
                    break;
                }
            }
        }
        if (counter + std::to_string(*it).length() > digits){
            std::cout << std::to_string(*it).substr(0, std::to_string(*it).length() - (counter + std::to_string(*it).length() - digits));
            counter = digits;
        } else {
            std::cout << *it++;
            counter += std::to_string(*it).length();
        }
        not_first = true;
    }
}

BitLongNum karatsuba_mul(const BitLongNum& x, const BitLongNum& y) { //Karatsubas mult implementation
    auto len = x.inner_repr.size();
    std::vector<int> res(len * 2);
    //создаем вектор избыточного размера, далее все операции - операции чтения и записи, выделения не происходит
    //создать этот вектор нам все же необходимо, так как далее мы можем использовать как результат, так и предшествующие ему значения

    if (len <= 100) {
        return x * y;
    }

    auto k = len / 2;

    std::vector<int> Xr{x.inner_repr.begin(), x.inner_repr.begin() + k};
    std::vector<int> Xl {x.inner_repr.begin() + k, x.inner_repr.end()};
    std::vector<int> Yr {y.inner_repr.begin(), y.inner_repr.begin() + k};
    std::vector<int> Yl {y.inner_repr.begin() + k, y.inner_repr.end()};

    BitLongNum P1 = karatsuba_mul(Xl, Yl);
    BitLongNum P2 = karatsuba_mul(Xr, Yr);

    std::vector<int> Xlr(k);
    std::vector<int> Ylr(k);

    for (int i = 0; i < k; ++i) {
        Xlr[i] = Xl[i] + Xr[i];
        Ylr[i] = Yl[i] + Yr[i];
    }

    BitLongNum P3 = karatsuba_mul(Xlr, Ylr);

    for (auto i = 0; i < len; ++i) {
        P3.inner_repr[i] -= P2.inner_repr[i] + P1.inner_repr[i];
    }

    for (auto i = 0; i < len; ++i) {
        res[i] = P2.inner_repr[i];
    }

    for (auto i = len; i < 2 * len; ++i) {
        res[i] = P1.inner_repr[i - len];
    }

    for (auto i = k; i < len + k; ++i) {
        res[i] += P3.inner_repr[i - k];
    }
    //при инициализации сделаем резерв
    return BitLongNum(res);
}

BitLongNum FasterFactorialKaratsuba(int left, int right){//algo based on idea of multiplying digit-equal nums
    if (right - left == 1){
        auto tmp_res_1 = BitLongNum(std::to_string(right));
        auto tmp_res_2 = BitLongNum(std::to_string(left));
        auto n = std::max(tmp_res_1.size(), tmp_res_2.size());
        tmp_res_1.extend(n);
        tmp_res_2.extend(n);
        //в базе рекурсии выделяем память под два представления и делаем экстенд(дополняем нулями)(оба далее будут использоваться в подсчете)
        return karatsuba_mul(tmp_res_1,  tmp_res_2);
    }
    if (right - left == 0){
        //в базе рекурсии выделяем память под одно представление
        return BitLongNum(std::to_string(right));
    }
    //тут рекурсивно вызываем функцию от меньших отрезков
    auto tmp_res_1 = FasterFactorialKaratsuba(left, (left + right) / 2);
    tmp_res_1.normalize();
    auto tmp_res_2 = FasterFactorialKaratsuba((left + right) / 2 + 1, right);
    tmp_res_2.normalize();
    auto n = std::max(tmp_res_1.size(), tmp_res_2.size());
    tmp_res_1.extend(n);
    tmp_res_2.extend(n);
    //на выходе из рекурсии будем получать новый результат и поднимать его наверх
    auto res =  karatsuba_mul(tmp_res_1,
                              tmp_res_2);
    res.normalize();
    return res;
}
#include <string>
#include <ctime>
#include <algorithm>
#include "Factorials.h"
#include <future>
#include <thread>


std::vector<int> Eratosphen(int n){ //basic eratosphen algo
    std::vector<bool> prime (n+1, true);
    std::vector<int> answer;
    prime[0] = prime[1] = false;
    for (size_t i=2; i<=n; ++i)
        if (prime[i])
            if (i * i <= n)
                for (int j= i * i; j <= n; j += i)
                    prime[j] = false;
    for (size_t i = 0; i < prime.size(); ++i){
        if (prime[i]){
            answer.push_back(i);
        }
    }
    return answer;
}

std::vector<std::pair<int, int>> Factorization(int n){ //Factorization of n
    std::vector<int> primes = Eratosphen(n);
    std::vector<std::pair<int, int>> result_of_factorization;
    for(const auto el: primes){
        std::pair<int, int> current_prime_param = {el, 0};
        int current_power = 1;
        int prime_in_power = el;
        while (prime_in_power <= n){
            int index = prime_in_power;
            while (index <= n){
                ++current_prime_param.second;
                index += prime_in_power;
            }
            prime_in_power *= el;
        }
        result_of_factorization.push_back(current_prime_param);
    }
    return result_of_factorization;
}

StringLongNum BinPow(StringLongNum& value, int deg){ //Fast power for possibly long values
    StringLongNum result("1");
    while (deg){
        if (deg % 2 == 0){
            deg /= 2;
            value = value * value;
        } else{
            --deg;
            result = result * value;
        }
    }
    return result;
}

StringLongNum GetPartFactorial(int start, int end,std::vector<std::pair<int, int>> factorization){
    StringLongNum result("1");
    std::string s;
    for (int i = start; i < end; ++i){
        StringLongNum tmp_res(std::to_string(factorization[i].first));
        tmp_res = BinPow(tmp_res, factorization[i].second);
        result = result * tmp_res ;
    }
    return result;
}


StringLongNum GetFactorialFromFactorization(int n) { //Factorial, based on idea of factorization into primes, which i love most
    std::vector<std::pair<int, int>> factorization = Factorization(n);
    StringLongNum result("1");
    std::future<StringLongNum> fut1 = std::async(std::launch::async, GetPartFactorial, 0, factorization.size() / 4,
                                           factorization);
    std::future<StringLongNum> fut2 = std::async(std::launch::async, GetPartFactorial, factorization.size() / 4,
                                           factorization.size() / 2, factorization);
    std::future<StringLongNum> fut3 = std::async(std::launch::async, GetPartFactorial, factorization.size() / 2,
                                           (factorization.size() * 3) / 4, factorization);
    std::future<StringLongNum> fut4 = std::async(std::launch::async, GetPartFactorial, (factorization.size() * 3) / 4,
                                           factorization.size(), factorization);

    result = fut1.get() * fut2.get() * fut3.get() * fut4.get();
//    return result;
//    for (const auto [num, pow]: factorization){
//        StringStringLongNum tmp_res(std::to_string(num));
//        tmp_res = BinPow(tmp_res, pow);
//        result = result * tmp_res ;
//    }
    return result;
}
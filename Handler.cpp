//
// Created by codeforcesrankzero on _20.09._20_2_2.
//
#include "Handler.h"
#include <iostream>
#include <ctime>
const std::string HELP = "Welcome to factorial ver. 1.0!!\n"
                         "Just write num from which you want to get factorial!\n"
                         "Also you can write how much digits yoy want to see\n"
                         "Correct format -- ./factorial {value} {number of digits(optionnaly)}\n"
                         "flag -wt means executing wout tests\n";
void Handler::RunTests(long long n, long long digits) {
    StringLongNum longNum(std::to_string(n));
    auto start_time_1 = std::clock();

    //выделяем память внутри функции под возвращаемое длинное число и н раз под множители
    StringLongNum res_1 = GetFactorial(longNum);
    auto end_time_1 = std::clock();
    double spent_1 = static_cast<double>(end_time_1 - start_time_1) / CLOCKS_PER_SEC;
    std::cout << spent_1 << " seconds of CPU time wasted on default factorial." << std::endl;
    std::cout << res_1.substr_out(digits) << std::endl;

    auto start_time_2 = std::clock();
    //тут стек рекурсии в нем кроме метаданных будем выделять место под факториалы для конкретных границ
    StringLongNum res_2 = FasterFactorial(2, n);
    auto end_time_2 = std::clock();
    double spent_2 = static_cast<double>(end_time_2 - start_time_2) / CLOCKS_PER_SEC;
    std::cout << spent_2 << " seconds of CPU time wasted on splitting factorial." << std::endl;
    std::cout << res_2.substr_out(digits) << std::endl;

    auto start_time_3 = std::clock();
    //тут будет выделение памяти под массив степеней простых в результате факторизации, а так же
    //   выделим память под результат и межрасчетные факториалы
    StringLongNum res_3 = GetFactorialFromFactorization(n);
    auto end_time_3 = std::clock();
    double spent_3 = static_cast<double>(end_time_3 - start_time_3) / CLOCKS_PER_SEC;
    std::cout << spent_3 << " seconds of CPU time wasted on factorial based on factorization into primes." << std::endl;
    std::cout << res_3.substr_out(digits) << std::endl;

    auto start_time_4 = std::clock();
    //тут происходит выделение памяти под длинное число(факториал) на каждом вызове рекурсии(как и во втором тесте)
    //   так же память выделится под результат
    auto res = FasterFactorialKaratsuba(2, n);
    auto end_time_4 = std::clock();
    //во всех тестах так же есть промежуточное выделение памяти во время умножения
    double spent_4 = static_cast<double>(end_time_4 - start_time_4) / CLOCKS_PER_SEC;
    std::cout << spent_4 << " seconds of CPU time wasted on factorial with Karatsuba multiplication." << std::endl;
    res.printRes(digits);

}

void Handler::HandleInput(int argc, char **argv) {
    if (argc > 4){
        std::cout << "Too many args!";
    } else if(argc == 4){
        long long n = std::atoi(argv[1]);
        long long digits = std::atoi(argv[2]);
        if (argv[3][1] == 'w') {
            SimplyCountFactorial(n, digits);
        } else{
            RunTests(n, digits);
        }
    }
    else if (argc == 3){
        long long n = std::atoi(argv[1]);
        if (argv[2][0] != '-') {
            long long digits = std::atoi(argv[2]);
            RunTests(n, digits);
        }
        else {
            if (argv[2][1] == 'w') {
                SimplyCountFactorial(n);
            } else{
                RunTests(n);
            }
        }
    } else if(argc == 2){
        long long n = std::atoi(argv[1]);
        RunTests(n);
    } else{
        std::cout << HELP << std::endl;
    }
}

void Handler::SimplyCountFactorial(long long n, long long digits) {
    BitLongNum longNum(std::to_string(n));
    BitLongNum res = FasterFactorialKaratsuba(2, n);
    res.printRes();
}



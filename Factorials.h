#include <iostream>
#include <vector>
#include "StringLongNum.h"

StringLongNum GetFactorial(StringLongNum& n);

StringLongNum FasterFactorial(int left, int right);

std::vector<int> Eratosphen(int n);

std::vector<std::pair<int, int>> Factorization(int n);

StringLongNum BinPow(StringLongNum& value, int deg);

StringLongNum GetFactorialFromFactorization(int n);
#pragma once
#include <iostream>
#include <vector>
#include <ios>

class BitLongNum{
private:
    std::vector<int> inner_repr;
public:

    size_t size();
    //в конструктор передаем ссылку, лишнего копирования не происходит
    BitLongNum(std::vector<int>& vec);
    //в конструктор передаем ссылку, лишнего копирования не происходит
    BitLongNum(const std::string& input_string);

    void normalize();

    void extend(int len);
    BitLongNum operator*(const BitLongNum& other) const;
    void printRes(int digits = 200);

    friend BitLongNum karatsuba_mul(const BitLongNum& x, const BitLongNum& y);


};

BitLongNum FasterFactorialKaratsuba(int left, int right);
#include <string>
#include <ctime>
#include <algorithm>
#include "Factorials.h"
#include <future>
#include <thread>
#include "StringLongNum.h"
StringLongNum StringLongNum::substr_out(size_t n) {
    //только чтение
    return StringLongNum(this->value.substr(0, n));
}

StringLongNum operator+ (const StringLongNum& left, const StringLongNum& right){ // default addition "v stolbik"
    //только чтение, возможен один пушбэк, но у нас зарезервирована память, так что реаллокации не будет
    int rest = 0;
    std::string result;
    long long left_idx = left.value.length() - 1;
    long long right_idx = right.value.length() - 1;
    bool IndexesAreFine = true;

    while (IndexesAreFine) {
        size_t current_value = 0;
        if (left_idx >= 0){
            current_value += left.value[left_idx] - '0'; // -'0' for char convert
        }
        if (right_idx >= 0){
            current_value += right.value[right_idx] - '0';
        }
        result.push_back((current_value + rest) % 10 + '0');
        rest = (current_value + rest)/ 10;
        if (left_idx >= 0){
            --left_idx;
        }
        if (right_idx >= 0){
            --right_idx;
        }
        if(left_idx == -1 and right_idx == -1){
            IndexesAreFine = false;
        }
    }
    if (rest != 0) {
        result.push_back((rest) + '0');
    }
    std::reverse(result.begin(), result.end());
    return StringLongNum(result);
}

StringLongNum operator*(const StringLongNum& left, const int right){ //mult by non long arithmetical val
    //реаллокации не происходит, так как в резалт резервируется большой кусок памяти
    StringLongNum result("");
    int rest = 0;
    for (int i = left.value.length() - 1; i >= 0; --i){
        int current_num = (left.value[i] - '0') * right + rest;
        result.value.push_back(current_num % 10 + '0');
        rest = current_num / 10;
    }
    if (rest != 0){
        result.value.push_back(rest + '0');
    }
    std::reverse(result.value.begin(), result.value.end() );
    return result;
}

StringLongNum operator*(const StringLongNum& left, const StringLongNum& right){ // mult of two possibly long values "stolbik" method
    //аналогично, происходит только одно выделение большого куска памяти
    StringLongNum result("0");
    int move_ptr_left = 0;
    for (long long j = right.value.length() - 1; j >= 0; --j){
        StringLongNum tmp_result = left * (right.value[j] - '0');
        for(size_t i = 0; i < move_ptr_left; ++i){
            tmp_result.value.push_back('0');
        }
        result = result + tmp_result;
        ++move_ptr_left;
    }
    return result;

}

std::ostream& operator<<(std::ostream& os, StringLongNum& longNum){
    //read only
    os << longNum.value;
    return os;
}

std::ostream& operator<<(std::ostream& os, StringLongNum longNum){
    //read only
    os << longNum.value;
    return os;
}


StringLongNum GetFactorial(StringLongNum& n){ //naive factorial
    //выделяемся н раз для умножения, внутри никаких реаллоков благодаря резерву
    StringLongNum init("1");
    StringLongNum result("1");
    n = n + StringLongNum("1");
    while (!(init == n)) {
        result = result * init;
        init = init + StringLongNum("1");
    }
    return result;
}

StringLongNum FasterFactorial(int left, int right){ //factorial based on idea of splitting and multiplication not in straight order
    //выделяемся примерно н раз, так же нет никаких реаллоков.
    if (right - left == 1){                   // which in terms of 5000 digits may not play its role
        return StringLongNum(std::to_string(right)) * StringLongNum(std::to_string(left));
    }
    if (right - left == 0){
        return StringLongNum(std::to_string(right));
    }
    return FasterFactorial(left, (left + right) / 2) * FasterFactorial((left + right) / 2 + 1, right);
}
